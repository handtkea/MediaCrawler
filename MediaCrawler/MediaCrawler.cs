﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class MediaCrawler
    {

        // Constants.
        private static readonly string[] MediaTypesAudio = { ".mp3", ".ogg", ".aac", ".wav" };
        private static readonly string[] MediaTypesVideo = { ".mp4", ".avi", ".mpeg", ".mkv", ".mov" };
        private static readonly string[] MediaTypesPicture = { ".jpg", ".png", ".tiff", ".ico" };

        // Enums.
        public enum MediaTypes
        {
            Audio, Video, Picture
        }

        // Public methods

        /// <summary>
        /// Returns media files in given folder filtered by given file extensions.
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="extensions"></param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        public static List<FileInfo> GetFilesByExtensionList(string folder, string[] extensions, bool recursive)
        {

            List<FileInfo> mFileInfos = new List<FileInfo>();

            try
            {
                DirectoryInfo dirInfo = new DirectoryInfo(folder);

                if (recursive)
                {
                    mFileInfos = dirInfo.EnumerateFiles("*.*", SearchOption.AllDirectories)
                        .Where(f => extensions.Contains(f.Extension.ToLower()))
                        .ToList<FileInfo>();
                }
                else
                {
                    mFileInfos = dirInfo.EnumerateFiles("*.*")
                        .Where(f => extensions.Contains(f.Extension.ToLower()))
                        .ToList<FileInfo>();
                }


                return mFileInfos;
            }
            catch
            {
                return mFileInfos;
            }

        }

        /// <summary>
        /// Returns media files of given type (Audio, Video, Picture) in given folder filtered by given file extensions.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="folder"></param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        public static List<FileInfo> GetFilesByType(MediaTypes type, string folder, bool recursive)
        {
            List<FileInfo> mFileInfos = new List<FileInfo>();

            string[] tmpTypes;

            try
            {

                switch (type)
                {
                    case MediaTypes.Audio:
                        {
                            tmpTypes = MediaTypesAudio;
                            break;
                        }

                    case MediaTypes.Video:
                        {
                            tmpTypes = MediaTypesVideo;
                            break;
                        }

                    case MediaTypes.Picture:
                        {
                            tmpTypes = MediaTypesPicture;
                            break;
                        }

                    default:
                        {
                            tmpTypes = MediaTypesVideo;
                            break;
                        }

                }

                mFileInfos = GetFilesByExtensionList(folder, tmpTypes, recursive);

                return mFileInfos;
            }
            catch
            {
                return mFileInfos;
            }
        }


        // Async gateways.
        public static async Task<List<FileInfo>> GetFilesByExtensionListAsync(string folder, string[] extensions, bool recursive)
        {

            List<FileInfo> mFileInfos = new List<FileInfo>();

            try
            {
                await Task.Run(() => mFileInfos = GetFilesByExtensionList(folder, extensions, recursive));

                return mFileInfos;
            }
            catch
            {
                return mFileInfos;
            }

        }

    }

}
